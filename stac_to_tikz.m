function stac_to_tikz(buslocations,casestruct,filename)

  branches = casestruct.branch;
  outfileid=fopen(filename,"w");

  buses = casestruct.bus;

  fprintf(outfileid,"\\documentclass{standalone}\n\\usepackage{tikz}\n\\begin{document}\n\\begin{tikzpicture}[every node/.style={scale=0.7,transform shape,circle}]\n");

  scale=[1,0.01,0.01];
  scaled_locations = bsxfun(@times,buslocations,scale);

  numberofnodes_vector=size(buslocations);
  numberofnodes=numberofnodes_vector(1);
  for i=1:numberofnodes
    type=0;
    for j = 1:size(buses)(1)
      if(buses(j,1) == scaled_locations(i,1))
        type=buses(j,2);
      end
    end

    if(type == 3)
      fprintf(outfileid,"\\node[draw=green] (%d) at (%f,%f) {%d};\n",scaled_locations(i,1),scaled_locations(i,2),scaled_locations(i,3),scaled_locations(i,1));
    end
    if(type == 2)
      fprintf(outfileid,"\\node[draw=blue] (%d) at (%f,%f) {%d};\n",scaled_locations(i,1),scaled_locations(i,2),scaled_locations(i,3),scaled_locations(i,1));
    end
    if(type == 1)
      fprintf(outfileid,"\\node[draw=red] (%d) at (%f,%f) {%d};\n",scaled_locations(i,1),scaled_locations(i,2),scaled_locations(i,3),scaled_locations(i,1));
    end

  end


  fprintf(outfileid,"\n\n");

  numberofbranches_vector=size(branches);
  numberofbranches=numberofbranches_vector(1);
  for i=1:numberofbranches
    fprintf(outfileid,"\\draw (%d) -- (%d);\n",branches(i,1),branches(i,2));
  end

  fprintf(outfileid,"\\end{tikzpicture}\n\\end{document}");


  fclose(outfileid);
end




